package com.luxoft.chatbot.telegrambot.model;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "QUESTIONS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Question {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "QUESTION")
	private String question;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "USER_QUESTION"
				, joinColumns = @JoinColumn(name = "QUESTION_ID")
				, inverseJoinColumns = @JoinColumn(name = "USER_ID"))
	private Set<User> users = new HashSet<>();
	
}
