package com.luxoft.chatbot.telegrambot.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "USERS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "FIRSTNAME")
	private String firstname;
	
	@Column(name = "LASTNAME")
	private String lastname;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "PHONE")
	private String phone;
	
	@Column(name = "VETERAN")
	private Boolean veteran;
	
	@Column(name = "FAMILY")
	private Long family;
	
	@Column(name = "BIRTHDAY")
	private LocalDate birthday;
	
	@Column(name = "LOCATION")
	private String location;

	@Column(name = "REGISTERED")
	private LocalDate registered;
	
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "users")
	private Set<Question> questions = new HashSet<>();
	
}
