package com.luxoft.chatbot.telegrambot.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.luxoft.chatbot.telegrambot.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	
	List<User> findByLocation(String location);

}
