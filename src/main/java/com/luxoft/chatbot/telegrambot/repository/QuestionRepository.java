package com.luxoft.chatbot.telegrambot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.luxoft.chatbot.telegrambot.model.Question;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Long> {

	@Query(value = "SELECT q.QUESTION "
					+ "FROM USER_QUESTION uq"
					+ "LEFT JOIN QUESTIONS q ON uq.QUESTION_ID = q.ID "
					+ "GROUP BY q.ID "
					+ "ORDER BY q.QUESTION DESC "
					+ "LIMIT 10", nativeQuery = true)
	List<Question> findTopQuestions();
}
