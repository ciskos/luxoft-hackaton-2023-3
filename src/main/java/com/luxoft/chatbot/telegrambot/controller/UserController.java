package com.luxoft.chatbot.telegrambot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luxoft.chatbot.telegrambot.model.User;
import com.luxoft.chatbot.telegrambot.service.UserService;

@RestController
@RequestMapping(value = UserController.USERS_PATH, produces = "application/json")
public class UserController {

	static final String USERS_PATH = "/users";
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/{location}")
	public ResponseEntity<List<User>> getUsersById(@PathVariable("location") String location)  {
		var users = userService.getUserByLocation(location);
		
		if (users != null) {
			return ResponseEntity.ok(users);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
}
