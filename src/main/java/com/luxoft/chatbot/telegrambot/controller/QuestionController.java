package com.luxoft.chatbot.telegrambot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luxoft.chatbot.telegrambot.model.Question;
import com.luxoft.chatbot.telegrambot.service.QuestionService;

@RestController
@RequestMapping(value = QuestionController.QUESTIONS_PATH, produces = "application/json")
public class QuestionController {

	static final String QUESTIONS_PATH = "/questions";
	
	@Autowired
	private QuestionService questionService;
	
	@GetMapping("/top")
	public ResponseEntity<List<Question>> getTopQuestions()  {
		var questions = questionService.getTopQuestions();
		
		if (questions != null) {
			return ResponseEntity.ok(questions);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
}
