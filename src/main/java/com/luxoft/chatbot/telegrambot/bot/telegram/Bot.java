package com.luxoft.chatbot.telegrambot.bot.telegram;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

public class Bot extends TelegramLongPollingBot {

	private static final String BACK = "back";
	private static final String NEXT = "next";
	private static final String COMMAND_MENU = "/menu";
	private static final String COMMAND_WHISPER = "/question";

	@Override
	public void onUpdateReceived(Update update) {
		var message = update.getMessage();
		var messageId = message.getMessageId();
		var fromUser = message.getFrom();
		var fromId = fromUser.getId();
		var fromFirstName = fromUser.getFirstName();
		var text = message.getText();

		var next = InlineKeyboardButton.builder()
				.text("Next")
				.callbackData(NEXT)
				.build();
		var back = InlineKeyboardButton.builder()
				.text("Back")
				.callbackData(BACK)
				.build();
		var url = InlineKeyboardButton.builder()
				.text("Tutorial")
				.url("https://core.telegram.org/bots/api")
				.build();

	}

	@Override
	public String getBotUsername() {
		return "LuxoftHackaton2023_bot";
	}

	@Override
	public String getBotToken() {
		// Тут должен вставляться токен. Без него бот не будет подключаться к системе.
		return "6311598043:AAH7gcUux6B5Sf8oedjxEhRPDJX-RJ8q0QY";
	}

}
