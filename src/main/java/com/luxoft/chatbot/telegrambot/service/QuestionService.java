package com.luxoft.chatbot.telegrambot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luxoft.chatbot.telegrambot.model.Question;
import com.luxoft.chatbot.telegrambot.repository.QuestionRepository;

@Service
public class QuestionService {

	@Autowired
	private QuestionRepository questionRepository;
	
	public List<Question> getTopQuestions() {
		return questionRepository.findTopQuestions();
	}

}
