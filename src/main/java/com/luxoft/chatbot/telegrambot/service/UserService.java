package com.luxoft.chatbot.telegrambot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luxoft.chatbot.telegrambot.model.User;
import com.luxoft.chatbot.telegrambot.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public List<User> getUserByLocation(String location) {
		return userRepository.findByLocation(location);
	}

}
